//
//  CharacterDetailView.swift
//  redspace-challenge
//
//  Created by Philippe Blanchette on 2022-03-11.
//

import SwiftUI

struct CharacterDetailView: View {
    
    var character:CharacterModel
    
    var body: some View {
        ScrollView {
            AsyncImage(url: URL(string:character.thumbnail), content: { phase in
                switch phase {
                case .success(let image):
                    image
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                case .failure:
                    Image(systemName: "exclamationmark.circle")
                        .thumbnail()
                        .foregroundColor(.red)
                case .empty:
                    ProgressView()
                        .frame(minWidth:100,minHeight:100)
                @unknown default:
                    Image(systemName: "questionmark.circle")
                        .thumbnail()
                }
            })
                .cornerRadius(10)
            Text("Name: \(character.name)")
            Text("Status: \(character.status)")
            Text("Species: \(character.species)")
            Text("Gender: \(character.gender)")
            Text("Current Location: \(character.currentLocation)")
        }
    }
}
