//
//  CharacterModel.swift
//  redspace-challenge
//
//  Created by Philippe Blanchette on 2022-03-11.
//

import Foundation

struct CharacterModel:Codable, Hashable {
    let id:Int
    let name:String
    let thumbnail:String
    let status:String
    let episodeCount:Int
    let species:String
    let gender:String
    let currentLocation:String
    
    init(from RMjsonResponse: RMNetworkAPI.CharacterResponse) {
        
        self.id = RMjsonResponse.id
        self.name = RMjsonResponse.name
        self.thumbnail = RMjsonResponse.image
        self.status = RMjsonResponse.status
        self.episodeCount = RMjsonResponse.episode.count
        self.species = RMjsonResponse.species
        self.gender = RMjsonResponse.gender
        self.currentLocation = RMjsonResponse.location.name
        
    }
    
}
