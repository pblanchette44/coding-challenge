//
//  Image+Extensions.swift
//  redspace-challenge
//
//  Created by Philippe Blanchette on 2022-03-13.
//

import Foundation
import SwiftUI

extension Image {
    
    public func thumbnail(dim:CGFloat = 100) -> some View {
        self.resizable()
            .aspectRatio(contentMode: .fit)
            .frame(maxWidth:dim,maxHeight:dim)
    }
    
}
