//
//  String+Extensions.swift
//  redspace-challenge
//
//  Created by Philippe Blanchette on 2022-03-13.
//

import Foundation

extension String {
    
    func sanitizeForAPI() -> String {
        self.trimmingCharacters(in: .whitespacesAndNewlines).replacingOccurrences(of: " ", with: "+")
    }
    
}
