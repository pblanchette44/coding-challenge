//
//  RMNetworkAPI.swift
//  redspace-challenge
//
//  Created by Philippe Blanchette on 2022-03-11.
//

import Foundation



class RMNetworkAPI {
    
    static let baseUrl:String = "https://rickandmortyapi.com/api"
    static let characterEndpoint = "/character"

    struct RMApiRequestHeader {
        let httpMethod:String
        let contentType:String = "Application/json"
    }
    
    struct CharacterListResponse: Codable {
        let results:[CharacterResponse]
    }
        
    struct CharacterResponse: Codable {
        let id:Int
        let name:String
        let status:String
        let species:String
        let gender:String
        let image: String
        let location: CharacterLocation
        let episode:[String]
    }
    
    struct CharacterLocation: Codable {
        let name:String
    }
    
    static func fetchCharacters(withFilter filter:String?, completion: @escaping (Result<CharacterListResponse,Error>) -> Void) {
        
        //Apply params
        var urlString:String = "\(RMNetworkAPI.baseUrl)\(RMNetworkAPI.characterEndpoint)"
        
        if let filter = filter?.sanitizeForAPI() {
            urlString.append(contentsOf: "?name=\(filter)")
        }
        
        //Format url
        guard let url = URL(string: urlString) else {
            fatalError("Fatal Error while building the URL for fetchCharacter")
        }

            
        RMNetworkAPI
            .executeDatatask(
                forURl: url,
                withHeader: RMApiRequestHeader(
                    httpMethod: "GET"
                )
            ) { data, response, error in
                    if let error = error {
                        completion(.failure(error))
                        return
                    }

                    if let data = data {
                        do {
                            let characterListJson = try JSONDecoder().decode(CharacterListResponse.self, from: data)
                            completion(.success(characterListJson))
                        } catch {
                            print("fetchCharacter: Error while decoding the response from the server")
                            completion(.failure(error))
                        }
                    }
            }
    }
    
    static func executeDatatask(
        forURl url:URL,
        withHeader header:RMApiRequestHeader,
        andCompletionHandler completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void
    ) {
        
        do {
            var request = URLRequest(url: url)
            request.httpMethod = header.httpMethod
            request.setValue(header.contentType, forHTTPHeaderField: "Content-Type")
            
            let session = URLSession.shared
            session.dataTask(
                with: request,
                completionHandler: completionHandler
            )
            .resume()
        }
    }
}
