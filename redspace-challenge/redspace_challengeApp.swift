//
//  redspace_challengeApp.swift
//  redspace-challenge
//
//  Created by Philippe Blanchette on 2022-03-11.
//

import SwiftUI

@main
struct redspace_challengeApp: App {
    var body: some Scene {
        WindowGroup {
            CharacterListView()
        }
    }
}
