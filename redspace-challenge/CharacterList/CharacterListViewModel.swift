//
//  CharacterListViewModel.swift
//  redspace-challenge
//
//  Created by Philippe Blanchette on 2022-03-11.
//

import Foundation


class CharacterListViewModel: ObservableObject {
    
    @Published var characterListModel:[CharacterModel] = []
    @Published var isLoading = false
    @Published var filter:String = "" {
        didSet {
            fetchData()
        }
    }
        
    func parseJsonToCharacterListModel(listResponse:RMNetworkAPI.CharacterListResponse) -> [CharacterModel] {
        return listResponse.results.map { CharacterModel(from: $0) }
    }
    
    func fetchData() {
        
        self.isLoading = true
        
        RMNetworkAPI.fetchCharacters(withFilter: filter.isEmpty ? nil : filter) { [weak self] response in
            
            guard let self = self else {
                fatalError("No ViewModel initialised")
            }
            
            DispatchQueue.main.async {
                switch response {
                case .success(let result):
                    self.characterListModel = self.parseJsonToCharacterListModel(listResponse: result)
                case .failure(let error):
                    self.characterListModel = []
                    print("Received Error from API: \(error.localizedDescription)")
                }
                
                self.isLoading = false
            }
        }
    }
    
}

