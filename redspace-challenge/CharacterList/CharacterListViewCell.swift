//
//  CharacterListViewCell.swift
//  redspace-challenge
//
//  Created by Philippe Blanchette on 2022-03-13.
//

import SwiftUI

struct CharacterListViewCell: View {
    
    let character:CharacterModel
    
    var body: some View {
        VStack {
            HStack {
                AsyncImage(url: URL(string:character.thumbnail), content: {phase in
                    switch phase {
                    case .success(let image):
                        image.thumbnail()
                    case .failure:
                        Image(systemName: "exclamationmark.circle")
                            .thumbnail()
                            .foregroundColor(.red)
                    case .empty:
                        ProgressView()
                            .frame(minWidth:100,minHeight:100)
                    @unknown default:
                        Image(systemName: "questionmark.circle")
                            .thumbnail()
                    }
                })
                    .cornerRadius(50.0)
                
                VStack(spacing: 20) {
                    Text(character.name)
                        .frame(maxWidth:.infinity,alignment: .leading)
                        .font(.title)
                    Text("Appears in \(character.episodeCount) episodes")
                        .frame(maxWidth:.infinity,alignment: .leading)
                        .font(.body)
                }
            }
        }
    }
}
