//
//  CharacterListView.swift
//  redspace-challenge
//
//  Created by Philippe Blanchette on 2022-03-11.
//

import SwiftUI

struct CharacterListView: View {
    
    @ObservedObject private var viewModel:CharacterListViewModel = CharacterListViewModel()

    var body: some View {
        NavigationView {
            LoaderView(isLoading: $viewModel.isLoading) {
                if viewModel.characterListModel.isEmpty {
                    Text("There is nothing here")
                } else {
                    List {
                        ForEach(viewModel.characterListModel, id:\.id) { character in
                            NavigationLink(destination: CharacterDetailView(character: character)) {
                                CharacterListViewCell(character: character)
                            }
                        }
                    }
                }
            }
        }
        .searchable(text: $viewModel.filter)
        .onAppear {
            viewModel.fetchData()
        }
    }
}

struct LoaderView<Content: View> : View {
    var isLoading:Binding<Bool>
    @ViewBuilder let content: () -> Content
    var body: some View {
        
        if isLoading.wrappedValue {
            ProgressView("Loading the infos from the Api")
        } else {
            content()
        }
        
    }
}







